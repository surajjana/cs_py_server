import json
from bottle import Bottle, run, route
import aiml
import requests
from pymongo import MongoClient
from bson.json_util import dumps
import os
import pymongo
import urllib
from random import randint
import operator
from collections import OrderedDict
from bs4 import BeautifulSoup
from alchemyapi import AlchemyAPI

API_KEY = "ra9apfchfyvrx29hztjakhy3"
URL = "https://api.gettyimages.com/v3/search/images/creative?phrase={text}"
DEFAULT_URL = "http://www.articlecube.com/sites/default/files/field/image/20906/career-development.jpg"


app = Bottle(__name__)
kernel = aiml.Kernel()
kernel.bootstrap(brainFile = "brain/bot_brain_advance.brn")

client = MongoClient('mongodb://52.34.226.223:27017/cs_bot')
db = client.cs_bot



@app.route('/')
def root():
	return 'Career Sensy Python Server Started'


@app.route('/bot_response/<msg>')
def bot_response(msg):
	res = kernel.respond(msg)
	res = res.replace('"','')
	return json.dumps({"status": "OK", "response": res, "msg_cat": "NLP", "msg_type":"text"})

@app.route('/bot_new_res/<sender>/<msg>')
def bot_new_res(sender, msg):
	cur = db.fb_msg_log.find({"fb_id":int(sender)}).sort('_id',pymongo.DESCENDING).limit(1)
	data = json.loads(dumps(cur))

	if (len(data) == 0):
		return {'status': 'false'}
	elif (data[0]['msg_cat'] == 'NLP' or data[0]['msg_cat'] == 'GS'):
		res = kernel.respond(msg)
		res = res.replace('"','')
		return {"status": "ok", "response": res, "msg_cat": "NLP"}
	elif (data[0]['msg_cat'] == 'holland'):
		q_id = randint(1,63)

		cur = db.questions.find({"id": q_id})
		data = json.loads(dumps(cur))

		return json.dumps({"status": "ok", "response": data[0]['value'], "msg_cat": data[0]['category']})
	else:
		r = requests.post('http://sentiment.vivekn.com/api/text/', {'txt': msg})
		res = str(json.loads(r.text)['result']['sentiment'])
		if (res == 'Neutral' or res == 'Positive'):
			cur =  db.fb_user_profile.update({ 'fb_id': int(sender) },{ '$inc':{ 'score.'+data[0]['msg_cat']: 1 } })
		
			q_id = randint(1,63)

			cur = db.questions.find({"id": q_id})
			data = json.loads(dumps(cur))

			return json.dumps({"status": "ok", "response": data[0]['value'], "msg_cat": data[0]['category']})
		elif (res == 'Negative'):
			q_id = randint(1,63)

			cur = db.questions.find({"id": q_id})
			data = json.loads(dumps(cur))

			return json.dumps({"status": "ok", "response": data[0]['value'], "msg_cat": data[0]['category']})
		else:
			res = kernel.respond(msg)
			res = res.replace('"','')
			return {"status": "ok", "response": res, "msg_cat": "NLP"}

@app.route('/next_response/<sender>/<msg_cat>')
def next_ques(sender, msg_cat):
	if(msg_cat == 'YO'):

		cur = db.fb_user_profile.update({ 'fb_id': int(sender) },{ '$set': {'score.R':0, 'score.I':0, 'score.A':0, 'score.S':0, 'score.E':0, 'score.C':0}})

		cur = db.questions.find({"category": "R"})
		data = json.loads(dumps(cur))
		print data
		return json.dumps({"status": "OK", "response": data[0]['value'], "msg_cat": data[0]['category'], "msg_type":"text"})
	elif(msg_cat == 'POS'):
		cur = db.fb_msg_log.find({"fb_id":int(sender)}).sort('_id',pymongo.DESCENDING).limit(1)
		data = json.loads(dumps(cur))

		print data

		cur =  db.fb_user_profile.update({ 'fb_id': int(sender) },{ '$inc':{ 'score.'+data[0]['msg_cat']: 1 } })
		
		q_id = randint(1,63)

		cur = db.questions.find({"id": q_id})
		data = json.loads(dumps(cur))

		return json.dumps({"status": "OK", "response": data[0]['value'], "msg_cat": data[0]['category'], "msg_type":"text"})
	elif(msg_cat == 'NEG' or msg_cat == 'NEU'):		
		q_id = randint(1,63)

		cur = db.questions.find({"id": q_id})
		data = json.loads(dumps(cur))

		return json.dumps({"status": "OK", "response": data[0]['value'], "msg_cat": data[0]['category'], "msg_type":"text"})

@app.route('/threshold/<sender>')
def threshold(sender):
	cur = db.fb_user_profile.find({"fb_id": int(sender)})
	data = json.loads(dumps(cur))

	if(len(data) == 0):
		return {'status': 'false'}

	res_data = sorted(data[0]['score'], key=data[0]['score'].__getitem__, reverse=True)

	a = OrderedDict()

	for i in res_data:
		a[i] = data[0]['score'][i]

	holland_six = ['R', 'I', 'A', 'S', 'E', 'C']

	h_final = ['realistic', 'investigative', 'artistic', 'social', 'enterprising', 'conventional']

	print a

	keys_1 = a.keys()[:2]

	

	if (a[keys_1[0]] >= 2 and a[keys_1[1]] != 0):
			
		r_1 = h_final[holland_six.index(keys_1[0])]
		r_2 = h_final[holland_six.index(keys_1[1])]

		return {'status': 'ok', 'holland': 'You are '+r_1+' and '+r_2+' person.. Check out following carrer opportunities ;-)', 'holland_value': keys_1}
	else:
		return {'status': 'false'}

@app.route('/req_skills/<job_id>')
def req_skills(job_id):
	skill_set_list = []
 
	try:
		html_content = requests.get("https://www.onetonline.org/link/summary/"+job_id, verify=False).content
	except Exception as e:
		html_content = ""

	# print html_content
			
	soup = BeautifulSoup(html_content, 'html.parser')

	for skill in soup.find_all("div", class_="section_Skills"):
		for item in skill.find_all('li'):
			skill_set_list.append(item.find("b").string)

	if(len(skill_set_list) == 0):
		return {'status': 'false'}
	else:
		return {'status': 'ok', 'skills': skill_set_list}

def get_images(search_phrase):
	headers = { "Api-Key": API_KEY}
	resp = requests.get(URL.format(text=search_phrase), headers=headers)
	return json.loads(resp.content).get("images")[0]

@app.route('/get_img/<name>')
def get_img(name):
	return {'status': 'ok', 'img_url': get_images(name).get("display_sizes", [{}])[0].get("uri", DEFAULT_URL)}

@app.get('/detect/<img_url>')
def detect(img_url):

	# face_url = 'http://52.34.226.223/sparse/img/face/'+img_url

	face_url = img_url.replace(' ', '/')

	# print face_url

	# return face_url

	alchemyapi = AlchemyAPI()

	response = alchemyapi.faceTagging('url', face_url)

	if response['status'] == 'OK':
	   
	    print(json.dumps(response, indent=4))

	    return response

	else:
	    print('Error in image extraction call: ', response['statusInfo'])

	    return {"Error":"'+response['statusInfo']+'"}

# run(host='0.0.0.0',port=8080,debug=True,reloader=True)