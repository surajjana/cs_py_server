import aiml
import api_engine

# Create the kernel and learn AIML files
kernel = aiml.Kernel()
kernel.learn("aiml/basic.aiml")
kernel.respond("load aiml b")
# kernel.bootstrap(brainFile = "brain/bot_brain_advance.brn")

while True:
	msg = raw_input("You: ")
	if msg == 'exit':
		kernel.saveBrain("bot_brain.brn")
		print 'Bot: Bye'
		exit()
	elif msg == "save":
		kernel.saveBrain("brain/bot_brain.brn")
	else:
		response = kernel.respond(msg)    
		print 'Bot:',response